import {element, flatt} from  '../src/index.js';

test('Has element creation helper', async () => {
	const el = element('name', {
		attr1: 'value1',
		attr2: undefined,
		attr3: true,
		class: flatt(['big', undefined, null, 'red', 0]),
		'data-attr': '<3'
	}, 'Body');

	expect(await el.render()).toMatchSnapshot();
});

test('Void tags don\'t close', async () => {
	const el = element('img', { src: 'example' });

	expect(await el.render()).toMatchSnapshot();
});
