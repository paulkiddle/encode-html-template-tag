import encodeHtml from '../src/encode-html';

test('encodes html', ()=>{
	expect(encodeHtml('<a href="//example.com/?a=1&b=2">`It\'s fine by me`</a>')).toMatchSnapshot();
});

test('works with non-strings', ()=>{
	expect(encodeHtml(5)).toEqual('5');
});
