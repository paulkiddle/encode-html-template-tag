import Template, { render } from  '../src/template';

test('Encodes async variables', async () => {
	const c = new Template(
		['<lit-a>\n', '<lit-b>\n'],
		[Promise.resolve('<val-1>')]
	);

	const out = c.render();

	expect(out).toEqual(expect.any(Promise));
	expect(await out).toMatchSnapshot();
});

test('Joins arrays', async () => {
	const c = new Template(
		['[lit-a]\n', '[lit-b]\n'],
		[[1,2,3]],
		val => `{encoded ${val}}\n`
	);
	expect(await c.render()).toMatchSnapshot;
});

test('Has sensible defaults', async () => {
	expect(await (new Template().render())).toMatchSnapshot();
});


test('Has a `with` function', () => {
	const t = new Template();

	const t2 = t.with({a: 1});

	expect(t2.a).toBe(1);

	expect(Object.getPrototypeOf(t2)).toBe(t);
});


test('Flattens netested templtaes', async() => {
	const t = new Template(['Nested template (',')'], [1]);
	const t2 = new Template(['Top template {\n\t', '\n}'], [t]);

	expect(await t2.render()).toMatchSnapshot();
});

test('Replaces values', async() => {
	const t = new Template(['Nested template (',')'], [1]);

	expect(await t.render(v=>v*2)).toMatchSnapshot();
});

test('Renders string', async()=>{
	expect(await render('<test>')).toMatchSnapshot();
});
