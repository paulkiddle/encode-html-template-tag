import Tag from '../src/tag';

test('Statically creates tag function', () => {
	expect(Tag.create(()=>{})).toMatchSnapshot();
});

test('Creates component', async () => {
	const tag = Tag.create();
	expect(await tag`One${'<'}three`.render()).toMatchSnapshot();
});

test('Joins values', async ()=> {
	expect(await Tag.create().join([1,2], ':').render()).toMatchSnapshot();
});

test('Join with default joiner', async ()=> {
	expect(await Tag.create().join([1,2]).render()).toMatchSnapshot();
});

test('Join works with empty array', async ()=> {
	expect(await Tag.create().join([], ':').render()).toMatchSnapshot();
});

test('Makes safe values', async ()=> {
	expect(await Tag.create().safe('<a>').render()).toMatchSnapshot();
});
