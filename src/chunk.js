export default class HtmlChunk {
	#source;

	constructor(source) {
		this.#source = String(source);
	}

	toString() {
		return this.#source;
	}
}