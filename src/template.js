export const htmlIterator = Symbol.for('htmlIterator');
import encodeHtml from './encode-html.js';

const isIterable = value => {
	// Strings are actually iterables, but we ignore them for our purposes
	if(!value || typeof value !== 'object' || value instanceof String) {
		return false;
	}

	if(Symbol.iterator in value || Symbol.asyncIterator in value) {
		return true;
	}

	return false;
};

export async function render(value, ...args){
	if(!value || !value[htmlIterator]) {
		value = new Template(['',''], [value]);
	}

	let str = '';
	for await(const chunk of value[htmlIterator](...args)) {
		str += chunk;
	}
	return str;
}

class Template {
	// #literals
	// #variables

	/**
	 * Create an object representing the template and its values.
	 * @param {Array} literals Parts of the template that will not be encoded
	 * @param {Array} variables Parts of the template that will be encoded
	 */
	constructor(literals = [''], variables = []) {
		this._literals = literals;
		this._variables = variables;
	}


	/**
	 * Tag this template with extra properites
	 * May be useful in conjunction with certain replacer functions
	 * @param {Object} props Properties to assign
	 * @returns {Object} A new object with the passed properties, and the template as the prototype
	 */
	with(props) {
		return Object.assign(
			Object.create(this),
			props
		);
	}

	async *_renderValue(value, replacer) {
		value = await (replacer ? replacer(value) : value);

		if(isIterable(value)) {
			for await(const v of value) {
				yield* this._renderValue(v, replacer);
			}
		} else /*if (value != null)*/ {
			yield value;
		}
	}

	/**
	 * Yields an async iterator that renders the template to a string.
	 * Template variables are handled in the following way:
	 *	- Strings will have html characters replaced with their html entities
	 *	- Nested Templates will be rendered
	 * 	- Promises will be resolved
	 * 	- Iterables will be flattened (including arrays, sync and async iterators)
	 * A `replace` function can be passed to alter individual variables before they are encoded.
	 * If supplied, this replace function will be passed down to all nested templates.
	 * @param {Function} replace Function that can be used to replace individual variables
	 * @returns {AsyncIterator}
	 */
	async *generate(replace){
		const [lit0, ...literals] = this._literals;
		const variables = this._variables;
		const encode = encodeHtml;

		yield lit0;

		for(let i=0; i<variables.length; i++) {
			for await(const value of this._renderValue(variables[i], replace)) {
				if(value && value[htmlIterator]) {
					yield* value[htmlIterator](replace);
				} else {
					yield encode(value);
				}
			}
			yield literals[i];
		}
	}

	async *[htmlIterator](replace){
		yield* this.generate(replace);
	}

	/**
	 * Render the template to a string. If any of the variables are Promises, return value will be a promise.
	 * Otherwise the return value will be a string.
	 * @param {Function} replace Function that can be used to replace individual variables
	 */
	async render(replacer){
		return render(this, replacer);
	}
}

export default Template;
