import HtmlChunk from './chunk.js';
import { encodeSync } from './sync.js';

async function * encode(value) {
	value = await value;

	if(!value) {
		return;
	}

	if(value instanceof HtmlStream) {
		yield * value.toStrings();
	} else if(value[Symbol.asyncIterator] || (value[Symbol.iterator] && typeof value !== 'string')) {
		for await(const item of value) {
			yield* encode(item);
		}
	} else {
		yield * encodeSync(value);
	}
}


async function * interleave(lits, ...vars){
	const len = vars.length;
	for(let i=0; i<len; i++) {
		yield lits[i];
		yield* encode(vars[i]);
	}

	yield lits[len];
}

class HtmlStream {
	#lits;
	#vars;

	constructor(lits, vars){
		this.#lits = lits;
		this.#vars = vars;
	}

	async * [Symbol.asyncIterator](){
		for await(const value of this.toStrings()) {
			yield new HtmlChunk(value);
		}
	}

	async * toStrings(){
		for await(const value of interleave(this.#lits, ...this.#vars)) {
			yield value;
		}
	}

	async concat() {
		return new HtmlChunk(await stringify(this));
	}
}

async function stringify(value) {
	let body = '';

	for await(const chunk of encode(value)) {
		body += chunk;
	}

	return body;
}

export default function html(lits, ...vars) {
	return new HtmlStream(lits, vars);
}

html.stream = encode;
html.stringify = stringify;
