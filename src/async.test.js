import { expect, test } from '@jest/globals';
import html from './async.js';
import HtmlChunk from './chunk.js';

test('Async template', async()=>{
	const nested = html`<b>${[html`Nested`]}${null}</b>`;
	expect((await nested[Symbol.asyncIterator]().next()).value).toBeInstanceOf(HtmlChunk);
	async function* iterator(){
		yield 'value <3';
		yield nested;
	}
	expect((
		await html`${iterator()}${[1,2,3]}`.concat()
	).toString()).toMatchSnapshot();
});