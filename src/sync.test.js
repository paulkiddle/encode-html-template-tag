import { expect, test } from '@jest/globals';
import html from './sync.js';

test('Sync tag', async()=>{
	expect(html.stringify(html`${['Test']}${void 0}!`)).toMatchSnapshot();
});