/**
 * @module encode-html-template-tag
 */

import Template from './template.js';

class Tag {
	static create(...args){
		const tagger = new this(...args);

		const tag = tagger.tag.bind(tagger);
		tag.join = tagger.join.bind(tagger);
		tag.safe = tagger.safe.bind(tagger);

		return tag;
	}

	/**
	 * Template tag function that returns a new template object
	 * @param {Array} lits Literals values to use in the template
	 * @param  {...any} values Variables to use in the template
	 */
	tag(lits, ...values) {
		return new Template(
			lits,
			values
		);
	}

	/**
	 * Create a Template that joins an array of values together
	 * @param {Array} parts Values to join
	 * @param {*} separator Literal to use as the joining string
	 */
	join(parts, separator = ',') {
		if (parts.length < 1) {
			return this.tag(['']);
		}

		return this.tag(['', ...Array(parts.length - 1).fill(separator), ''], ...parts);
	}

	/**
	 * Cast the value to a Template so that it can be rendered without encoding it
	 * @param {*} value Value to cast
	 */
	safe(value) {
		return this.tag([String(value)]);
	}
}

export default Tag;
