export { htmlIterator, render } from './template.js';

import Tag from './tag.js';

const htmlTagger = Tag.create();

/**
 * Tag function for your html template
 *
 * ```javascript
 * html`There are ${numberOfLights} lights`
 * ```
 *
 * @returns {Template} The template object
 */
const html = htmlTagger;

/**
 * Creates a Template that joins together an array of values with an optional separator
 *
 * ```javascript
 * html.join([1,2,3,4,5], ' and ')
 * ```
 *
 * @param {Array} values Values to join
 * @param {String} separator Separator to use, defaults to `,`
 * @returns {Template}
 */
const join = html.join = htmlTagger.join;

/**
 * Creates a new Tempalte using a value that won't be encoded when rendered, or passed as a value to another template.
 * Don't use this for unfiltered user input.
 *
 * ```javascript
 * html.safe(markdownToHtml('# Page title\n\n*Hello!*'));
 * ```
 * @param {String} value The value to use as a literal
 * @returns {Template}
 */
const safe = html.safe = htmlTagger.safe;

/**
 * Alias of html.safe
 * @see html.safe
 */
const raw = html.raw = html.safe;

export default html;

export {
	join, raw, safe
};

import createElementHelper from './element.js';

/**
 * Helper to create an individual element
 * Void elements are hardcoded and do not accept children.
 * @example element('a', { href: 'http://example.com' }, 'Click here for example dot com').render();
 * @param {string} name The name of the tag to create
 * @param {Object} attributes The dictionary of attributes for the element.
 * This should be an object whose keys are the attribute names and whose values are the attribute values.
 * Attributes with values that are null, undefined, or false will not be added to the output.
 * Attributes with values that are `true` will only have the attribute names present in the output, with no value.
 * All other values will be encoded on output.
 * @param {...children} children The children to put inside the tag.
 * @returns {Template}
 */
export const element = createElementHelper(html);
export const flatt = element.flatt;
