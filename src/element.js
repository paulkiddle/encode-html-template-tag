const isFnullish = value => ((value??false)===false);
const voidTags = ['area', 'base', 'br', 'col', 'embed', 'hr', 'img', 'input', 'link', 'meta', 'param', 'source', 'track', 'wbr'];

function createElementHelper(html) {
	const attributes = object =>
		Object.entries(object)
			.map(
				([key, value])=> {
					return isFnullish(value) ? '' : (
						value === true ? html` ${key}` : html` ${key}="${value}"`
					);
				}
			);

	const voidElement = (name, attrs) => html`<${name}${attributes(attrs)} />`;
	const nonVoidElement = (name, attrs, ...children) => html`<${name}${attributes(attrs)}>${children}</${name}>`;

	const element = (name, ...etc) => (voidTags.includes(name.toLowerCase()) ? voidElement : nonVoidElement)(name, ...etc);

	element.void = voidElement;
	element.nonVoid = voidElement;
	element.flatt = (...values) => html.join(values.flat().filter(x=>!isFnullish(x)), ' ');

	return element;
}

export default createElementHelper;
