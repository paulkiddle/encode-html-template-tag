import HtmlChunk from './chunk.js';
import encodeHtml from './encode-html.js';

export function * encodeSync(value) {
	if(!value) {
		return;
	}

	if(value instanceof HtmlChunk) {
		yield value.toString();
	} else if (typeof value !== 'string' && value[Symbol.iterator]){
		for(const item of value) {
			yield* encodeSync(item);
		}
	} else {
		yield encodeHtml(value);
	}
}

function * interleave(lits, ...vars){
	const len = vars.length;
	for(let i=0; i<len; i++) {
		yield lits[i];
		yield* encodeSync(vars[i]);
	}

	yield lits[len];
}

function concat(iterator) {
	return Array.from(iterator).join('');
}

export function stringify(value) {
	return concat(encodeSync(value));
}

export default function html(lits, ...vars) {
	return new HtmlChunk(concat(interleave(lits, ...vars)));
}

html.stringify = stringify;
