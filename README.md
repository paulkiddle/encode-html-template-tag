# encode-html-template-tag

A simple but flexible template tagging function.

 - Create templates that automatically encode passed-in variables
 - Nest templates without fear of double encoding them
 - Handles variables that are arrays, promises, or combinations
 - Replace variables at render time
 - Create a tag that uses a custom encoding function

With thanks to the developers of escape-html-template-tag for the inspiration.

## Changes in version 2

 - Private fields have been removed so this package now works in
browsers and earlier node versions.
 - The `render()` function now always returns a promise
 - Introduced a `generate()` function that returns an async iterable

## How to use

There are two main ways to generate tempaltes: as a promise and as an async iterator

### Promise

Perform the transformations and concatenate all into one string using the `render` function.

```javascript
const html = require('encode-html-template-tag');

const message = 'I <3 horses';
const para = html`<p>${message}</p>`;
await para.render() // <p>I &lt;3 horses</p>;
```

### Async iterator

THe `generate` function returns an async iterator.
Useful in node JS when dealing with streams:

```javascript
const { Readable } = require('stream');
const html = require('encode-html-template-tag');

module.exports = async (req, res) => {
	const template = html`Thanks for visiting, the date and time is ${new Date())}.`;

	Readable.from(template.generate()).pipe(res);
};
```



## Examples

```javascript
const html = require('encode-html-template-tag');

const div = html`<div>${para}</div>`;
await div.render() // <div><p>I &lt;3 horses</p></div>

const promise = Promise.resolve('Hello!');
const array = [1,2,Promise.resolve(3)];

const component = html`<b>${promise}</b><i>${array}</i>`;
await component.render(); // <b>Hello!</b><i>123</i>

// Create a translatable data type
const t = text => {
	return {
		translatable: true,
		toString(){ return text; }
	}
}

const welcome = html`<p>${t('Welcome')}</p>`;

// We can inspect and modify any inserted variables
// using the render function's callback.
// This includes values in nested components.
await welcome.render(value => {
	if(value.translatable) {
		value = translateIntoFrench(value);
	}
	return value;
}) // <p>Bienvenue</p>
```

## Api

## Dependencies
None
## Modules

<dl>
<dt><a href="#module_encode-html-template-tag">encode-html-template-tag</a></dt>
<dd></dd>
</dl>

## Classes

<dl>
<dt><a href="#Template">Template</a></dt>
<dd></dd>
</dl>

## Constants

<dl>
<dt><a href="#html">html</a> ⇒ <code><a href="#Template">Template</a></code></dt>
<dd><p>Tag function for your html template</p>
<pre><code class="language-javascript">html`There are ${numberOfLights} lights`
</code></pre>
</dd>
<dt><a href="#join">join</a> ⇒ <code><a href="#Template">Template</a></code></dt>
<dd><p>Creates a Template that joins together an array of values with an optional separator</p>
<pre><code class="language-javascript">html.join([1,2,3,4,5], &#39; and &#39;)
</code></pre>
</dd>
<dt><a href="#safe">safe</a> ⇒ <code><a href="#Template">Template</a></code></dt>
<dd><p>Creates a new Tempalte using a value that won&#39;t be encoded when rendered, or passed as a value to another template.
Don&#39;t use this for unfiltered user input.</p>
<pre><code class="language-javascript">html.safe(markdownToHtml(&#39;# Page title\n\n*Hello!*&#39;));
</code></pre>
</dd>
<dt><a href="#raw">raw</a></dt>
<dd><p>Alias of html.safe</p>
</dd>
<dt><a href="#element">element</a> ⇒ <code><a href="#Template">Template</a></code></dt>
<dd><p>Helper to create an individual element
Void elements are hardcoded and do not accept children.</p>
</dd>
</dl>

<a name="module_encode-html-template-tag"></a>

## encode-html-template-tag
<a name="Template"></a>

## Template
**Kind**: global class  

* [Template](#Template)
    * [new Template(literals, variables)](#new_Template_new)
    * [.with(props)](#Template+with) ⇒ <code>Object</code>
    * [.generate(replace)](#Template+generate) ⇒ <code>AsyncIterator</code>
    * [.render(replace)](#Template+render)

<a name="new_Template_new"></a>

### new Template(literals, variables)
Create an object representing the template and its values.


| Param | Type | Description |
| --- | --- | --- |
| literals | <code>Array</code> | Parts of the template that will not be encoded |
| variables | <code>Array</code> | Parts of the template that will be encoded |

<a name="Template+with"></a>

### template.with(props) ⇒ <code>Object</code>
Tag this template with extra properites
May be useful in conjunction with certain replacer functions

**Kind**: instance method of [<code>Template</code>](#Template)  
**Returns**: <code>Object</code> - A new object with the passed properties, and the template as the prototype  

| Param | Type | Description |
| --- | --- | --- |
| props | <code>Object</code> | Properties to assign |

<a name="Template+generate"></a>

### template.generate(replace) ⇒ <code>AsyncIterator</code>
Yields an async iterator that renders the template to a string.
Template variables are handled in the following way:
	- Strings will have html characters replaced with their html entities
	- Nested Templates will be rendered
	- Promises will be resolved
	- Iterables will be flattened (including arrays, sync and async iterators)
A `replace` function can be passed to alter individual variables before they are encoded.
If supplied, this replace function will be passed down to all nested templates.

**Kind**: instance method of [<code>Template</code>](#Template)  

| Param | Type | Description |
| --- | --- | --- |
| replace | <code>function</code> | Function that can be used to replace individual variables |

<a name="Template+render"></a>

### template.render(replace)
Render the template to a string. If any of the variables are Promises, return value will be a promise.
Otherwise the return value will be a string.

**Kind**: instance method of [<code>Template</code>](#Template)  

| Param | Type | Description |
| --- | --- | --- |
| replace | <code>function</code> | Function that can be used to replace individual variables |

<a name="html"></a>

## html ⇒ [<code>Template</code>](#Template)
Tag function for your html template

```javascript
html`There are ${numberOfLights} lights`
```

**Kind**: global constant  
**Returns**: [<code>Template</code>](#Template) - The template object  
<a name="join"></a>

## join ⇒ [<code>Template</code>](#Template)
Creates a Template that joins together an array of values with an optional separator

```javascript
html.join([1,2,3,4,5], ' and ')
```

**Kind**: global constant  

| Param | Type | Description |
| --- | --- | --- |
| values | <code>Array</code> | Values to join |
| separator | <code>String</code> | Separator to use, defaults to `,` |

<a name="safe"></a>

## safe ⇒ [<code>Template</code>](#Template)
Creates a new Tempalte using a value that won't be encoded when rendered, or passed as a value to another template.
Don't use this for unfiltered user input.

```javascript
html.safe(markdownToHtml('# Page title\n\n*Hello!*'));
```

**Kind**: global constant  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>String</code> | The value to use as a literal |

<a name="raw"></a>

## raw
Alias of html.safe

**Kind**: global constant  
**See**: html.safe  
<a name="element"></a>

## element ⇒ [<code>Template</code>](#Template)
Helper to create an individual element
Void elements are hardcoded and do not accept children.

**Kind**: global constant  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The name of the tag to create |
| attributes | <code>Object</code> | The dictionary of attributes for the element. This should be an object whose keys are the attribute names and whose values are the attribute values. Attributes with values that are null, undefined, or false will not be added to the output. Attributes with values that are `true` will only have the attribute names present in the output, with no value. All other values will be encoded on output. |
| ...children | <code>children</code> | The children to put inside the tag. |

**Example**  
```js
element('a', { href: 'http://example.com' }, 'Click here for example dot com').render();
```
