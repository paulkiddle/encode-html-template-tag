## Modules

<dl>
<dt><a href="#module_encode-html-template-tag">encode-html-template-tag</a></dt>
<dd></dd>
<dt><a href="#module_encode-html-template-tag">encode-html-template-tag</a></dt>
<dd></dd>
<dt><a href="#module_encode-html-template-tag">encode-html-template-tag</a></dt>
<dd></dd>
</dl>

<a name="module_encode-html-template-tag"></a>

## encode-html-template-tag

* [encode-html-template-tag](#module_encode-html-template-tag)
    * _static_
        * [.element](#module_encode-html-template-tag.element) ⇒ <code>Template</code>
    * _inner_
        * [~Tag](#module_encode-html-template-tag..Tag)
            * [new Tag(encoder)](#new_module_encode-html-template-tag..Tag_new)
            * [.tag(lits, ...values)](#module_encode-html-template-tag..Tag+tag)
            * [.join(parts, separator)](#module_encode-html-template-tag..Tag+join)
            * [.safe(value)](#module_encode-html-template-tag..Tag+safe)
        * [~Template](#module_encode-html-template-tag..Template)
            * [new Template(literals, variables, encode)](#new_module_encode-html-template-tag..Template_new)
            * [.with(props)](#module_encode-html-template-tag..Template+with) ⇒ <code>Object</code>
            * [.generate(replace)](#module_encode-html-template-tag..Template+generate) ⇒ <code>AsyncIterator</code>
            * [.render(replace)](#module_encode-html-template-tag..Template+render)
        * [~html](#module_encode-html-template-tag..html) ⇒ <code>Template</code>
        * [~join](#module_encode-html-template-tag..join) ⇒ <code>Template</code>
        * [~safe](#module_encode-html-template-tag..safe) ⇒ <code>Template</code>
        * [~raw](#module_encode-html-template-tag..raw)

<a name="module_encode-html-template-tag.element"></a>

### encode-html-template-tag.element ⇒ <code>Template</code>
Helper to create an individual element

**Kind**: static constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The name of the tag to create |
| attributes | <code>Object</code> | The dictionary of attributes for the element. This should be an object whose keys are the attribute names and whose values are the attribute values. Attributes with values that are null, undefined, or false will not be added to the output. Attributes with values that are `true` will only have the attribute names present in the output, with no value. All other values will be encoded on output. |
| ...children | <code>children</code> | The children to put inside the tag. |

**Example**  
```js
element('a', { href: 'http://example.com' }, 'Click here for example dot com').render();
```
<a name="module_encode-html-template-tag..Tag"></a>

### encode-html-template-tag~Tag
**Kind**: inner class of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

* [~Tag](#module_encode-html-template-tag..Tag)
    * [new Tag(encoder)](#new_module_encode-html-template-tag..Tag_new)
    * [.tag(lits, ...values)](#module_encode-html-template-tag..Tag+tag)
    * [.join(parts, separator)](#module_encode-html-template-tag..Tag+join)
    * [.safe(value)](#module_encode-html-template-tag..Tag+safe)

<a name="new_module_encode-html-template-tag..Tag_new"></a>

#### new Tag(encoder)
Class that contains methods for creating Template objects


| Param | Type | Description |
| --- | --- | --- |
| encoder | <code>function</code> | Function for encoding non-literal values |

<a name="module_encode-html-template-tag..Tag+tag"></a>

#### tag.tag(lits, ...values)
Template tag function that returns a new template object

**Kind**: instance method of [<code>Tag</code>](#module_encode-html-template-tag..Tag)  

| Param | Type | Description |
| --- | --- | --- |
| lits | <code>Array</code> | Literals values to use in the template |
| ...values | <code>any</code> | Variables to use in the template |

<a name="module_encode-html-template-tag..Tag+join"></a>

#### tag.join(parts, separator)
Create a Template that joins an array of values together

**Kind**: instance method of [<code>Tag</code>](#module_encode-html-template-tag..Tag)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| parts | <code>Array</code> |  | Values to join |
| separator | <code>\*</code> | <code>,</code> | Literal to use as the joining string |

<a name="module_encode-html-template-tag..Tag+safe"></a>

#### tag.safe(value)
Cast the value to a Template so that it can be rendered without encoding it

**Kind**: instance method of [<code>Tag</code>](#module_encode-html-template-tag..Tag)  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | Value to cast |

<a name="module_encode-html-template-tag..Template"></a>

### encode-html-template-tag~Template
**Kind**: inner class of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

* [~Template](#module_encode-html-template-tag..Template)
    * [new Template(literals, variables, encode)](#new_module_encode-html-template-tag..Template_new)
    * [.with(props)](#module_encode-html-template-tag..Template+with) ⇒ <code>Object</code>
    * [.generate(replace)](#module_encode-html-template-tag..Template+generate) ⇒ <code>AsyncIterator</code>
    * [.render(replace)](#module_encode-html-template-tag..Template+render)

<a name="new_module_encode-html-template-tag..Template_new"></a>

#### new Template(literals, variables, encode)
Create an object representing the template and its values.


| Param | Type | Description |
| --- | --- | --- |
| literals | <code>Array</code> | Parts of the template that will not be encoded |
| variables | <code>Array</code> | Parts of the template that will be encoded |
| encode | <code>function</code> | The function that encodes the variables |

<a name="module_encode-html-template-tag..Template+with"></a>

#### template.with(props) ⇒ <code>Object</code>
Tag this template with extra properites
May be useful in conjunction with certain replacer functions

**Kind**: instance method of [<code>Template</code>](#module_encode-html-template-tag..Template)  
**Returns**: <code>Object</code> - A new object with the passed properties, and the template as the prototype  

| Param | Type | Description |
| --- | --- | --- |
| props | <code>Object</code> | Properties to assign |

<a name="module_encode-html-template-tag..Template+generate"></a>

#### template.generate(replace) ⇒ <code>AsyncIterator</code>
Yields an async iterator that renders the template to a string.
Template variables are handled in the following way:
	- Strings will have html characters replaced with their html entities
	- Nested Templates will be rendered
	- Promises will be resolved
	- Iterables will be flattened (including arrays, sync and async iterators)
A `replace` function can be passed to alter individual variables before they are encoded.
If supplied, this replace function will be passed down to all nested templates.

**Kind**: instance method of [<code>Template</code>](#module_encode-html-template-tag..Template)  

| Param | Type | Description |
| --- | --- | --- |
| replace | <code>function</code> | Function that can be used to replace individual variables |

<a name="module_encode-html-template-tag..Template+render"></a>

#### template.render(replace)
Render the template to a string. If any of the variables are Promises, return value will be a promise.
Otherwise the return value will be a string.

**Kind**: instance method of [<code>Template</code>](#module_encode-html-template-tag..Template)  

| Param | Type | Description |
| --- | --- | --- |
| replace | <code>function</code> | Function that can be used to replace individual variables |

<a name="module_encode-html-template-tag..html"></a>

### encode-html-template-tag~html ⇒ <code>Template</code>
Tag function for your html template

```javascript
html`There are ${numberOfLights} lights`
```

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  
**Returns**: <code>Template</code> - The template object  
<a name="module_encode-html-template-tag..join"></a>

### encode-html-template-tag~join ⇒ <code>Template</code>
Creates a Template that joins together an array of values with an optional separator

```javascript
html.join([1,2,3,4,5], ' and ')
```

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

| Param | Type | Description |
| --- | --- | --- |
| values | <code>Array</code> | Values to join |
| separator | <code>String</code> | Separator to use, defaults to `,` |

<a name="module_encode-html-template-tag..safe"></a>

### encode-html-template-tag~safe ⇒ <code>Template</code>
Creates a new Tempalte using a value that won't be encoded when rendered, or passed as a value to another template.
Don't use this for unfiltered user input.

```javascript
html.safe(markdownToHtml('# Page title\n\n*Hello!*'));
```

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>String</code> | The value to use as a literal |

<a name="module_encode-html-template-tag..raw"></a>

### encode-html-template-tag~raw
Alias of html.safe

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  
**See**: html.safe  
<a name="module_encode-html-template-tag"></a>

## encode-html-template-tag

* [encode-html-template-tag](#module_encode-html-template-tag)
    * _static_
        * [.element](#module_encode-html-template-tag.element) ⇒ <code>Template</code>
    * _inner_
        * [~Tag](#module_encode-html-template-tag..Tag)
            * [new Tag(encoder)](#new_module_encode-html-template-tag..Tag_new)
            * [.tag(lits, ...values)](#module_encode-html-template-tag..Tag+tag)
            * [.join(parts, separator)](#module_encode-html-template-tag..Tag+join)
            * [.safe(value)](#module_encode-html-template-tag..Tag+safe)
        * [~Template](#module_encode-html-template-tag..Template)
            * [new Template(literals, variables, encode)](#new_module_encode-html-template-tag..Template_new)
            * [.with(props)](#module_encode-html-template-tag..Template+with) ⇒ <code>Object</code>
            * [.generate(replace)](#module_encode-html-template-tag..Template+generate) ⇒ <code>AsyncIterator</code>
            * [.render(replace)](#module_encode-html-template-tag..Template+render)
        * [~html](#module_encode-html-template-tag..html) ⇒ <code>Template</code>
        * [~join](#module_encode-html-template-tag..join) ⇒ <code>Template</code>
        * [~safe](#module_encode-html-template-tag..safe) ⇒ <code>Template</code>
        * [~raw](#module_encode-html-template-tag..raw)

<a name="module_encode-html-template-tag.element"></a>

### encode-html-template-tag.element ⇒ <code>Template</code>
Helper to create an individual element

**Kind**: static constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The name of the tag to create |
| attributes | <code>Object</code> | The dictionary of attributes for the element. This should be an object whose keys are the attribute names and whose values are the attribute values. Attributes with values that are null, undefined, or false will not be added to the output. Attributes with values that are `true` will only have the attribute names present in the output, with no value. All other values will be encoded on output. |
| ...children | <code>children</code> | The children to put inside the tag. |

**Example**  
```js
element('a', { href: 'http://example.com' }, 'Click here for example dot com').render();
```
<a name="module_encode-html-template-tag..Tag"></a>

### encode-html-template-tag~Tag
**Kind**: inner class of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

* [~Tag](#module_encode-html-template-tag..Tag)
    * [new Tag(encoder)](#new_module_encode-html-template-tag..Tag_new)
    * [.tag(lits, ...values)](#module_encode-html-template-tag..Tag+tag)
    * [.join(parts, separator)](#module_encode-html-template-tag..Tag+join)
    * [.safe(value)](#module_encode-html-template-tag..Tag+safe)

<a name="new_module_encode-html-template-tag..Tag_new"></a>

#### new Tag(encoder)
Class that contains methods for creating Template objects


| Param | Type | Description |
| --- | --- | --- |
| encoder | <code>function</code> | Function for encoding non-literal values |

<a name="module_encode-html-template-tag..Tag+tag"></a>

#### tag.tag(lits, ...values)
Template tag function that returns a new template object

**Kind**: instance method of [<code>Tag</code>](#module_encode-html-template-tag..Tag)  

| Param | Type | Description |
| --- | --- | --- |
| lits | <code>Array</code> | Literals values to use in the template |
| ...values | <code>any</code> | Variables to use in the template |

<a name="module_encode-html-template-tag..Tag+join"></a>

#### tag.join(parts, separator)
Create a Template that joins an array of values together

**Kind**: instance method of [<code>Tag</code>](#module_encode-html-template-tag..Tag)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| parts | <code>Array</code> |  | Values to join |
| separator | <code>\*</code> | <code>,</code> | Literal to use as the joining string |

<a name="module_encode-html-template-tag..Tag+safe"></a>

#### tag.safe(value)
Cast the value to a Template so that it can be rendered without encoding it

**Kind**: instance method of [<code>Tag</code>](#module_encode-html-template-tag..Tag)  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | Value to cast |

<a name="module_encode-html-template-tag..Template"></a>

### encode-html-template-tag~Template
**Kind**: inner class of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

* [~Template](#module_encode-html-template-tag..Template)
    * [new Template(literals, variables, encode)](#new_module_encode-html-template-tag..Template_new)
    * [.with(props)](#module_encode-html-template-tag..Template+with) ⇒ <code>Object</code>
    * [.generate(replace)](#module_encode-html-template-tag..Template+generate) ⇒ <code>AsyncIterator</code>
    * [.render(replace)](#module_encode-html-template-tag..Template+render)

<a name="new_module_encode-html-template-tag..Template_new"></a>

#### new Template(literals, variables, encode)
Create an object representing the template and its values.


| Param | Type | Description |
| --- | --- | --- |
| literals | <code>Array</code> | Parts of the template that will not be encoded |
| variables | <code>Array</code> | Parts of the template that will be encoded |
| encode | <code>function</code> | The function that encodes the variables |

<a name="module_encode-html-template-tag..Template+with"></a>

#### template.with(props) ⇒ <code>Object</code>
Tag this template with extra properites
May be useful in conjunction with certain replacer functions

**Kind**: instance method of [<code>Template</code>](#module_encode-html-template-tag..Template)  
**Returns**: <code>Object</code> - A new object with the passed properties, and the template as the prototype  

| Param | Type | Description |
| --- | --- | --- |
| props | <code>Object</code> | Properties to assign |

<a name="module_encode-html-template-tag..Template+generate"></a>

#### template.generate(replace) ⇒ <code>AsyncIterator</code>
Yields an async iterator that renders the template to a string.
Template variables are handled in the following way:
	- Strings will have html characters replaced with their html entities
	- Nested Templates will be rendered
	- Promises will be resolved
	- Iterables will be flattened (including arrays, sync and async iterators)
A `replace` function can be passed to alter individual variables before they are encoded.
If supplied, this replace function will be passed down to all nested templates.

**Kind**: instance method of [<code>Template</code>](#module_encode-html-template-tag..Template)  

| Param | Type | Description |
| --- | --- | --- |
| replace | <code>function</code> | Function that can be used to replace individual variables |

<a name="module_encode-html-template-tag..Template+render"></a>

#### template.render(replace)
Render the template to a string. If any of the variables are Promises, return value will be a promise.
Otherwise the return value will be a string.

**Kind**: instance method of [<code>Template</code>](#module_encode-html-template-tag..Template)  

| Param | Type | Description |
| --- | --- | --- |
| replace | <code>function</code> | Function that can be used to replace individual variables |

<a name="module_encode-html-template-tag..html"></a>

### encode-html-template-tag~html ⇒ <code>Template</code>
Tag function for your html template

```javascript
html`There are ${numberOfLights} lights`
```

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  
**Returns**: <code>Template</code> - The template object  
<a name="module_encode-html-template-tag..join"></a>

### encode-html-template-tag~join ⇒ <code>Template</code>
Creates a Template that joins together an array of values with an optional separator

```javascript
html.join([1,2,3,4,5], ' and ')
```

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

| Param | Type | Description |
| --- | --- | --- |
| values | <code>Array</code> | Values to join |
| separator | <code>String</code> | Separator to use, defaults to `,` |

<a name="module_encode-html-template-tag..safe"></a>

### encode-html-template-tag~safe ⇒ <code>Template</code>
Creates a new Tempalte using a value that won't be encoded when rendered, or passed as a value to another template.
Don't use this for unfiltered user input.

```javascript
html.safe(markdownToHtml('# Page title\n\n*Hello!*'));
```

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>String</code> | The value to use as a literal |

<a name="module_encode-html-template-tag..raw"></a>

### encode-html-template-tag~raw
Alias of html.safe

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  
**See**: html.safe  
<a name="module_encode-html-template-tag"></a>

## encode-html-template-tag

* [encode-html-template-tag](#module_encode-html-template-tag)
    * _static_
        * [.element](#module_encode-html-template-tag.element) ⇒ <code>Template</code>
    * _inner_
        * [~Tag](#module_encode-html-template-tag..Tag)
            * [new Tag(encoder)](#new_module_encode-html-template-tag..Tag_new)
            * [.tag(lits, ...values)](#module_encode-html-template-tag..Tag+tag)
            * [.join(parts, separator)](#module_encode-html-template-tag..Tag+join)
            * [.safe(value)](#module_encode-html-template-tag..Tag+safe)
        * [~Template](#module_encode-html-template-tag..Template)
            * [new Template(literals, variables, encode)](#new_module_encode-html-template-tag..Template_new)
            * [.with(props)](#module_encode-html-template-tag..Template+with) ⇒ <code>Object</code>
            * [.generate(replace)](#module_encode-html-template-tag..Template+generate) ⇒ <code>AsyncIterator</code>
            * [.render(replace)](#module_encode-html-template-tag..Template+render)
        * [~html](#module_encode-html-template-tag..html) ⇒ <code>Template</code>
        * [~join](#module_encode-html-template-tag..join) ⇒ <code>Template</code>
        * [~safe](#module_encode-html-template-tag..safe) ⇒ <code>Template</code>
        * [~raw](#module_encode-html-template-tag..raw)

<a name="module_encode-html-template-tag.element"></a>

### encode-html-template-tag.element ⇒ <code>Template</code>
Helper to create an individual element

**Kind**: static constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | The name of the tag to create |
| attributes | <code>Object</code> | The dictionary of attributes for the element. This should be an object whose keys are the attribute names and whose values are the attribute values. Attributes with values that are null, undefined, or false will not be added to the output. Attributes with values that are `true` will only have the attribute names present in the output, with no value. All other values will be encoded on output. |
| ...children | <code>children</code> | The children to put inside the tag. |

**Example**  
```js
element('a', { href: 'http://example.com' }, 'Click here for example dot com').render();
```
<a name="module_encode-html-template-tag..Tag"></a>

### encode-html-template-tag~Tag
**Kind**: inner class of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

* [~Tag](#module_encode-html-template-tag..Tag)
    * [new Tag(encoder)](#new_module_encode-html-template-tag..Tag_new)
    * [.tag(lits, ...values)](#module_encode-html-template-tag..Tag+tag)
    * [.join(parts, separator)](#module_encode-html-template-tag..Tag+join)
    * [.safe(value)](#module_encode-html-template-tag..Tag+safe)

<a name="new_module_encode-html-template-tag..Tag_new"></a>

#### new Tag(encoder)
Class that contains methods for creating Template objects


| Param | Type | Description |
| --- | --- | --- |
| encoder | <code>function</code> | Function for encoding non-literal values |

<a name="module_encode-html-template-tag..Tag+tag"></a>

#### tag.tag(lits, ...values)
Template tag function that returns a new template object

**Kind**: instance method of [<code>Tag</code>](#module_encode-html-template-tag..Tag)  

| Param | Type | Description |
| --- | --- | --- |
| lits | <code>Array</code> | Literals values to use in the template |
| ...values | <code>any</code> | Variables to use in the template |

<a name="module_encode-html-template-tag..Tag+join"></a>

#### tag.join(parts, separator)
Create a Template that joins an array of values together

**Kind**: instance method of [<code>Tag</code>](#module_encode-html-template-tag..Tag)  

| Param | Type | Default | Description |
| --- | --- | --- | --- |
| parts | <code>Array</code> |  | Values to join |
| separator | <code>\*</code> | <code>,</code> | Literal to use as the joining string |

<a name="module_encode-html-template-tag..Tag+safe"></a>

#### tag.safe(value)
Cast the value to a Template so that it can be rendered without encoding it

**Kind**: instance method of [<code>Tag</code>](#module_encode-html-template-tag..Tag)  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | Value to cast |

<a name="module_encode-html-template-tag..Template"></a>

### encode-html-template-tag~Template
**Kind**: inner class of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

* [~Template](#module_encode-html-template-tag..Template)
    * [new Template(literals, variables, encode)](#new_module_encode-html-template-tag..Template_new)
    * [.with(props)](#module_encode-html-template-tag..Template+with) ⇒ <code>Object</code>
    * [.generate(replace)](#module_encode-html-template-tag..Template+generate) ⇒ <code>AsyncIterator</code>
    * [.render(replace)](#module_encode-html-template-tag..Template+render)

<a name="new_module_encode-html-template-tag..Template_new"></a>

#### new Template(literals, variables, encode)
Create an object representing the template and its values.


| Param | Type | Description |
| --- | --- | --- |
| literals | <code>Array</code> | Parts of the template that will not be encoded |
| variables | <code>Array</code> | Parts of the template that will be encoded |
| encode | <code>function</code> | The function that encodes the variables |

<a name="module_encode-html-template-tag..Template+with"></a>

#### template.with(props) ⇒ <code>Object</code>
Tag this template with extra properites
May be useful in conjunction with certain replacer functions

**Kind**: instance method of [<code>Template</code>](#module_encode-html-template-tag..Template)  
**Returns**: <code>Object</code> - A new object with the passed properties, and the template as the prototype  

| Param | Type | Description |
| --- | --- | --- |
| props | <code>Object</code> | Properties to assign |

<a name="module_encode-html-template-tag..Template+generate"></a>

#### template.generate(replace) ⇒ <code>AsyncIterator</code>
Yields an async iterator that renders the template to a string.
Template variables are handled in the following way:
	- Strings will have html characters replaced with their html entities
	- Nested Templates will be rendered
	- Promises will be resolved
	- Iterables will be flattened (including arrays, sync and async iterators)
A `replace` function can be passed to alter individual variables before they are encoded.
If supplied, this replace function will be passed down to all nested templates.

**Kind**: instance method of [<code>Template</code>](#module_encode-html-template-tag..Template)  

| Param | Type | Description |
| --- | --- | --- |
| replace | <code>function</code> | Function that can be used to replace individual variables |

<a name="module_encode-html-template-tag..Template+render"></a>

#### template.render(replace)
Render the template to a string. If any of the variables are Promises, return value will be a promise.
Otherwise the return value will be a string.

**Kind**: instance method of [<code>Template</code>](#module_encode-html-template-tag..Template)  

| Param | Type | Description |
| --- | --- | --- |
| replace | <code>function</code> | Function that can be used to replace individual variables |

<a name="module_encode-html-template-tag..html"></a>

### encode-html-template-tag~html ⇒ <code>Template</code>
Tag function for your html template

```javascript
html`There are ${numberOfLights} lights`
```

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  
**Returns**: <code>Template</code> - The template object  
<a name="module_encode-html-template-tag..join"></a>

### encode-html-template-tag~join ⇒ <code>Template</code>
Creates a Template that joins together an array of values with an optional separator

```javascript
html.join([1,2,3,4,5], ' and ')
```

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

| Param | Type | Description |
| --- | --- | --- |
| values | <code>Array</code> | Values to join |
| separator | <code>String</code> | Separator to use, defaults to `,` |

<a name="module_encode-html-template-tag..safe"></a>

### encode-html-template-tag~safe ⇒ <code>Template</code>
Creates a new Tempalte using a value that won't be encoded when rendered, or passed as a value to another template.
Don't use this for unfiltered user input.

```javascript
html.safe(markdownToHtml('# Page title\n\n*Hello!*'));
```

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>String</code> | The value to use as a literal |

<a name="module_encode-html-template-tag..raw"></a>

### encode-html-template-tag~raw
Alias of html.safe

**Kind**: inner constant of [<code>encode-html-template-tag</code>](#module_encode-html-template-tag)  
**See**: html.safe  
