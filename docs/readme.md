# encode-html-template-tag

A simple but flexible template tagging function.

 - Create templates that automatically encode passed-in variables
 - Nest templates without fear of double encoding them
 - Handles variables that are arrays, promises, or combinations
 - Replace variables at render time
 - Create a tag that uses a custom encoding function

With thanks to the developers of escape-html-template-tag for the inspiration.

Scroll down for usage examples, or see the [API docs](./api.md) for full details.

## Changes in version 2

 - Private fields have been removed so this package now works in
browsers and earlier node versions.
 - The `render()` function now always returns a promise
 - Introduced a `generate()` function that returns an async iterable

## How to use

There are two main ways to generate tempaltes: as a promise and as an async iterator

### Promise

Perform the transformations and concatenate all into one string using the `render` function.

```javascript
const html = require('encode-html-template-tag');

const message = 'I <3 horses';
const para = html`<p>${message}</p>`;
await para.render() // <p>I &lt;3 horses</p>;
```

### Async iterator

THe `generate` function returns an async iterator.
Useful in node JS when dealing with streams:

```javascript
const { Readable } = require('stream');
const html = require('encode-html-template-tag');

module.exports = async (req, res) => {
	const template = html`Thanks for visiting, the date and time is ${new Date())}.`;

	Readable.from(template.generate()).pipe(res);
};
```



## Examples

```javascript
const html = require('encode-html-template-tag');

const div = html`<div>${para}</div>`;
await div.render() // <div><p>I &lt;3 horses</p></div>

const promise = Promise.resolve('Hello!');
const array = [1,2,Promise.resolve(3)];

const component = html`<b>${promise}</b><i>${array}</i>`;
await component.render(); // <b>Hello!</b><i>123</i>

// Create a translatable data type
const t = text => {
	return {
		translatable: true,
		toString(){ return text; }
	}
}

const welcome = html`<p>${t('Welcome')}</p>`;

// We can inspect and modify any inserted variables
// using the render function's callback.
// This includes values in nested components.
await welcome.render(value => {
	if(value.translatable) {
		value = translateIntoFrench(value);
	}
	return value;
}) // <p>Bienvenue</p>
```

## Api
